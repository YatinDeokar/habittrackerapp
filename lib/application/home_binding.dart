


import 'package:get/get.dart';
import 'package:habit_tracker_app/application/home_controller.dart';
import 'package:habit_tracker_app/domain/i_repository.dart';
import 'package:habit_tracker_app/infrastructure/repository.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IRepository>(
          () => Repository(),
    );
    Get.lazyPut<HomeController>(
          () => HomeController(
          repository: Get.find<IRepository>()),
    );
  }
}