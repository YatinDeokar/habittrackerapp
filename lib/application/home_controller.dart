

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:habit_tracker_app/common/category.dart';
import 'package:habit_tracker_app/domain/i_repository.dart';

class HomeController extends GetxController{
  IRepository? repository;
  HomeController({this.repository});

  TextEditingController habitTitleController = TextEditingController();
  bool isBooksLoading = true;
  List<Category>? booksList;

  @override
  void onInit() {
  getBooksList();
  super.onInit();
  }

  //Fetch Book List data from api
  getBooksList(){

    repository?.fetchBooks().then((value){
      isBooksLoading = false;
      value.fold((l){
          booksList = [];
      }, (r){
          booksList = r;
          update();
      });
    });

  }

}

