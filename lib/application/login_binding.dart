import 'package:get/get.dart';
import 'package:habit_tracker_app/application/login_controller.dart';
import 'package:habit_tracker_app/domain/i_repository.dart';
import 'package:habit_tracker_app/infrastructure/repository.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IRepository>(
      () => Repository(),
    );
    Get.lazyPut<LoginController>(
      () => LoginController(
          repository: Get.find<IRepository>()),
    );
  }
}
