

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:habit_tracker_app/application/utils.dart';
import 'package:habit_tracker_app/domain/i_repository.dart';
import 'package:habit_tracker_app/routes/app_pages.dart';

class LoginController extends GetxController{
    IRepository? repository;
    LoginController({this.repository});

    TextEditingController emailController = TextEditingController();
    TextEditingController passController = TextEditingController();

    login(){

        if(validation()) {
          if (mockLoginError()) {

          Utils.loadingDialog();

          repository!.login(emailController.text, passController.text).then((
              value) {
            Utils.closeDialog();
            value.fold(
                    (error) {
                  Get.showSnackbar(const GetSnackBar(title: "Error",
                    message: "Email is Required",
                    duration: Duration(seconds: 2),));
                }, (success) {
              Get.showSnackbar(const GetSnackBar(title: "Success",
                  message: "Login Successful",
                  duration: Duration(seconds: 2)));
              Get.offAndToNamed(Routes.HOME_PAGE);
            });
          });
        }else{
            Future.delayed(const Duration(seconds: 3), (){
              Utils.closeDialog();
              Get.showSnackbar(const GetSnackBar(title: "Error", message: "Email or Password is Incorrect", duration: Duration(seconds: 2),));
            });
          }
        }

    }

    mockLoginError(){
      Utils.loadingDialog();
      return (emailController.text != "yatin@gmail.com" || passController.text != "Zoom123")
          ? false
          : true;


    }

   bool validation(){

        if(emailController.text.isEmpty) {
            Get.showSnackbar(const GetSnackBar(title: "Error", message: "Email is Required",duration: Duration(seconds: 2)));
          return false;
        }

        if(passController.text.isEmpty) {
            Get.showSnackbar(const GetSnackBar(title: "Error", message: "Password is Required",duration: Duration(seconds: 2)));
            return false;
        }

        return true;
    }

}