
class Category {
  final String name;
  final String price;
  final String image;

  Category(this.name, this.price, this.image);
}

//Convert Json to Object
List<Category> categories = categoriesData
    .map((item) => Category(item['name'].toString(), item['price'].toString(), item['image'].toString()))
    .toList();


//Mock Api data
var categoriesData = [
  {
    "name": "The my book cover",
    'price': 17,
    'image': "assets/images/book1.jpg"
  },
  {"name": "The Namesake", 'price': 25, 'image': "assets/images/book2.jpg"},
  {"name": "State of wonder", 'price': 13, 'image': "assets/images/book3.jpg"},
  {"name": "Inferno", 'price': 17, 'image': "assets/images/book4.jpg"},
];