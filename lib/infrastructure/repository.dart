


import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:habit_tracker_app/common/api_endpoints.dart';
import 'package:habit_tracker_app/common/category.dart';
import 'package:habit_tracker_app/domain/i_repository.dart';

class Repository extends IRepository{


  @override
  Future<Either<String, String>> login(String userId, String password) async{
    try {

      Map<String, dynamic> data = {"id":0,"userName":"$userId","password":"$password"};

      var response = await Dio().post(
        ApiEndpoints.loginApi,
        data: jsonEncode(data),
      );

      if(response.statusCode == 200) {
        return right("success");
      }

      return left(response.statusMessage!);

    } catch (e) {
      return left(e.toString());
    }
  }

  @override
  Future<Either<String, List<Category>>> fetchBooks() async{
    try {
      //Mock api request
     await Future.delayed(const Duration(seconds: 4), (){

      }).then((value) {
       return right(categories);
     });

     return right(categories);
    } catch (e) {
      return left("Something Went");
    }
  }


}