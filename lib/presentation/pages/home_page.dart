import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:habit_tracker_app/application/home_controller.dart';

import '../../common/constants.dart';

class HomeScreen extends StatelessWidget {
   const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(left: 20, top: 50, right: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SvgPicture.asset("assets/icons/menu.svg"),
                const SizedBox(
                    height: 40,
                    width: 40,
                    child: Icon(Icons.supervised_user_circle_rounded, color: Colors.amber, size: 40,)),
              ],
            ),
            const SizedBox(height: 30),
            const Text("Hey Yatin,", style: kHeadingextStyle),
            const Text("Find a book you want to read",
                style: kSubheadingextStyle),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 30),
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
              height: 60,
              width: double.infinity,
              decoration: BoxDecoration(
                color: const Color(0xFFF5F5F7),
                borderRadius: BorderRadius.circular(40),
              ),
              child: Row(
                children: <Widget>[
                  SvgPicture.asset("assets/icons/search.svg"),
                  const SizedBox(width: 16),
                  const Text(
                    "Search for anything",
                    style: TextStyle(
                      fontSize: 18,
                      color: Color(0xFFA0A5BD),
                    ),
                  )
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                const Text("Category", style: kTitleTextStyle),
                Text(
                  "See All",
                  style: kSubtitleTextSyule.copyWith(color: kBlueColor),
                ),
              ],
            ),
            const SizedBox(height: 30),
            Expanded(
              child: GetBuilder<HomeController>(
                  builder: (_){
                    return _.isBooksLoading
                        ? const Center(child: SizedBox(height: 50, width: 50,child: CircularProgressIndicator()))
                        : (_.booksList == null || _.booksList!.isEmpty)
                        ? const Center(child: Text("No Data Found"))
                        : MasonryGridView.count(
                      padding: const EdgeInsets.all(0),
                      crossAxisCount: 2,
                      itemCount: _.booksList?.length,
                      crossAxisSpacing: 20,
                      mainAxisSpacing: 20,
                      itemBuilder: (context, index) {
                        return Column(children: <Widget>[
                          Container(
                            padding: const EdgeInsets.all(20),
                            height: index.isEven ? 200 : 240,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(16),
                              image: DecorationImage(
                                image: AssetImage(_.booksList![index].image),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 18,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                _.booksList![index].name,
                                style: kTitleTextStyle,
                              ),
                              Text(
                                'Price: \$${_.booksList![index].price}',
                                style: TextStyle(
                                  color: kTextColor.withOpacity(.5),
                                ),
                              )
                            ],
                          ),
                        ]);
                      },
                      // staggeredTileBuilder: (index) => const StaggeredTile.fit(1),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
