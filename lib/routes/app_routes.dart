part of 'app_pages.dart';

abstract class Routes {
  static const String LOGIN_PAGE = _Paths.LOGIN_PAGE;
  static const String HOME_PAGE = _Paths.HOME_PAGE;
  static const String DETAILS_PAGE = _Paths.DETAILS_PAGE;
  static const String ADD_HABIT_PAGE = _Paths.ADD_HABIT_PAGE;
}

abstract class _Paths {
  static const String LOGIN_PAGE = '/login_page';
  static const String HOME_PAGE = '/home_page';
  static const String DETAILS_PAGE = '/details_page';
  static const String ADD_HABIT_PAGE = '/add_habit_form';

}
